lazy val root = (project in file(".")).enablePlugins(PlayScala)
  .settings(
    name := "",
    organization := "com.myntra",
    scalaVersion := "2.11.8",
    version := "0.1.0-SNAPSHOT"
  )

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(jdbc, ehcache, ws, specs2 % Test, guice)
      