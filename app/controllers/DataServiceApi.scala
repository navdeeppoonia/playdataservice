package controllers

import javax.inject.Inject
import models.Query
import play.api.libs.json._
import play.api.mvc.{AbstractController, ControllerComponents}

class DataServiceApi @Inject()(cc: ControllerComponents) extends AbstractController(cc) {


  def query = Action { request =>

    implicit val rr: OFormat[Query] = Json.using[Json.WithDefaultValues].format[Query]

    val rj: JsResult[Query] = Json.fromJson[Query](request.body.asJson.get)

    rj match {
      case JsSuccess(r: Query, path: JsPath) => println("Name: " + r.query + "id is " + r.id)
      case e: JsError => println("Errors: " + JsError.toJson(e).toString())
    }
    Ok(Json.toJson(rj.get))
  }

}
