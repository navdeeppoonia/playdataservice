package models

case class Query(
                  id: String = "",
                  query: String,
                  tableName: String,
                  startTs: String,
                  endTs: String
                )